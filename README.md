# @sb-plugins/sb-geometry-plugin

@sb-plugins/sb-geometry-plugin (sihem bouhenniche geometry plugin) is Plugin that contains some basic mathematical operations about calculating surface of some geometric shapes.

Full tutorial at : https://medium.com/@fs_bouhenniche/create-your-first-npm-package-and-publish-it-into-gitlab-registry-94eec41e0770

[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://developer.mozilla.org/fr/docs/Web/JavaScript)

# 💻 Repository

-   Checkout Git repository at : <https://gitlab.com/sihemBouh/sb-geometry-plugin>
-   Issues & Feedback : <fs_bouhenniche@esi.dz>

# 😎 Contributors

-   Sihem BOUHENNICHE

# 📝 Guideline and installation

-   To install this package you should :

    1. Create `.npmrc` file at the root of your project.
    2. The `.npmrc` should contain this content :

    ```
    https://gitlab.com/sihemBouh/sb-geometry-plugin
    ```

    3. Type to install :

    ```
    npm i @sb-plugins/sb-geometry-plugin
    ```

# 🎉 How to use methods

-   Import desired methods and use it directly:

```js
import { getRectangleArea } from '@sb-plugins/sb-geometry-plugin';

const getRectangleArea = getRectangleArea(1, 1);
```

-   You can check documentation at : <https://sihembouh.gitlab.io/sb-geometry-plugin/>
