const { getCircleArea } = require('../src/index');

test('test if getCircleArea return a truthy value', () => {
    expect(getCircleArea(1)).toBeTruthy();
});

test('Calculate area of circle with rayon = 1, result expected : 1*1*pi = pi', () => {
    expect(getCircleArea(1)).toBe(Math.PI);
});
