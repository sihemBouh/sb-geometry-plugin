const { getTriangleArea } = require('../src/index');

test('test if getTriangleArea return a truthy value', () => {
    expect(getTriangleArea(1, 1)).toBeTruthy();
});

test('Calculate area of triangle with b = 1 h = 2, result expected : 1*2*0.5 = 1', () => {
    expect(getTriangleArea(1, 2)).toBe(1);
});
