const { getRectangleArea } = require('../src/index');

test('test if getRectangleArea return a truthy value', () => {
    expect(getRectangleArea(1, 1)).toBeTruthy();
});

test('Calculate area of rectangle with b = 2 h = 2, result expected : 2*2 = 4', () => {
    expect(getRectangleArea(2, 2)).toBe(4);
});
