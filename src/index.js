const { getCircleArea } = require('./methods/circleArea');
const { getRectangleArea } = require('./methods/rectangleArea');
const { getTriangleArea } = require('./methods/triangleArea');

module.exports = {
    getCircleArea,
    getRectangleArea,
    getTriangleArea,
};
