/**
 * Calculate rectangle area
 * @param {*} length
 * @param {*} width
 * @returns {number} area
 */
function getRectangleArea(length, width) {
    return length * width;
}
module.exports = { getRectangleArea };
