/**
 * Calculate triangle area
 * @param {*} base
 * @param {*} perpendicularHight
 * @returns area
 */
function getTriangleArea(base, perpendicularHight) {
    return base * perpendicularHight * 0.5;
}

module.exports = { getTriangleArea };
